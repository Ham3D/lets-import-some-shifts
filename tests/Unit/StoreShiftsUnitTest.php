<?php

namespace Tests\Unit;


use App\Exceptions\ShiftHandlerNotFoundException;
use App\Services\StoreShifts;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StoreShiftsUnitTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test
     * @throws ShiftHandlerNotFoundException
     */
    public function importShifts(): void
    {
        $storeShift = new StoreShifts();
        $storeShift->handler($this->readJsonSample()['shifts']);
        
        $this->assertDatabaseCount('shifts', 5);
        $this->assertDatabaseHas('users', ['email' => 'dummy+282@dummy.com']);
        $this->assertDatabaseHas('departments', ['title' => 'WWW Riko Dep (do not use)']);
        $this->assertDatabaseHas('events', ['name' => 'Customer requested event [ school ]']);
    }
    
    private function readJsonSample(): array
    {
        $fileName = base_path('tests').'/sampleShifts.json';
        $json     = file_get_contents($fileName);
        
        return json_decode($json, true);
    }
}
