<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StoreShiftsApiTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function storeShifts(): void
    {
        $response = $this->postJson('/api/shifts', $this->readJsonSample());
        
        $response->assertStatus(200);
        $response->assertJson(['status' => 'done']);
        $this->assertDatabaseCount('shifts', 5);
        $this->assertDatabaseHas('users', ['email' => 'dummy+282@dummy.com']);
        $this->assertDatabaseHas('departments', ['title' => 'WWW Riko Dep (do not use)']);
        $this->assertDatabaseHas('events', ['name' => 'Customer requested event [ school ]']);
    }
    
    /** @test */
    public function queryShiftsWithFoundResult(): void
    {
        $this->postJson('/api/shifts', $this->readJsonSample());
        
        $secondQuery = $this->post('/api/search',
            [
                "location" => "Lambeth Palace",
                "from"     => "2018-1-1T00:00:00+01:00",
                "to"       => "2018-12-23T00:00:00+01:00",
            ]);
        $secondQuery->assertSuccessful();
        $secondQuery->assertJsonFragment(['location' => 'Lambeth Palace']);
        
        
    }
    
    /** @test */
    public function queryShiftsWithOutResults(): void
    {
        $this->postJson('/api/shifts', $this->readJsonSample());
        
        $query = $this->post('/api/search',
            [
                "location" => "Tartu Dev House",
                "from"     => "2018-1-1T00:00:00+01:00",
                "to"       => "2018-12-23T00:00:00+01:00",
            ]);
        $query->assertSuccessful();
        $query->assertJson(['shifts' => []]);
    }
    
    /** @test */
    public function destroyData()
    {
        // add some data
        $this->postJson('/api/shifts', $this->readJsonSample());
        
        $response = $this->delete('/api/shifts');
        $response->assertJsonFragment(['status' => 'done']);
        $this->assertDatabaseCount('shifts', 0);
    }
    
    private function readJsonSample()
    {
        $fileName = base_path('tests').'/sampleShifts.json';
        $json     = file_get_contents($fileName);
        
        return json_decode($json, true);
    }
}
