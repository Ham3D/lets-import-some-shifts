<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\Console\Exception\RuntimeException;
use Tests\TestCase;

class StoreShiftsCliTest extends TestCase
{
    use RefreshDatabase;
    
    /** @test */
    public function useCommandWithoutProvidingFile(): void
    {
        $this->expectException(RuntimeException::class);
        
        $this->artisan('storeShifts');
    }
    
    /** @test */
    public function importShiftsFromFile(): void
    {
        $fileName = base_path('tests').'/sampleShifts.json';
        $command  = $this->artisan("storeShifts $fileName");
        $command->assertExitCode(0);
        
        $command->expectsOutput('done.');
    }
}
