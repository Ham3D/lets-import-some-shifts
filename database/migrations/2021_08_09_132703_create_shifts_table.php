<?php

use App\Models\Location;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shifts', function (Blueprint $table) {
            $table->id();
            $table->string('type');
            $table->timestamp('start');
            $table->timestamp('end');
            $table->foreignIdFor(User::class);
            $table->foreignIdFor(Location::class)->nullable();
            $table->foreignIdFor(\App\Models\Event::class)->nullable();
            $table->integer('rate')->nullable();
            $table->integer('charge')->nullable();
            $table->string('area')->nullable();
            $table->integer('temp_key')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shifts');
    }
}
