Parim Trial Test  
-  
how to run The Code :  
  
- copy .env.example to .env  
- create a database and config .env  
- import postman json file to test API  
- unzip data.7z for testing cli
- composer install  
- done  
  
## API  
**We have three Apis:**  
- Store shifts  
- Delete shifts  
- Query shifts  
  
### Store shifts  
- I coded StoreShift class in a general way, you can feed it with controller and api and also from cli and command line.  
- I also created a command to feed a Monster size shift json to this class to see the results.  
- there is a simple builder on StoreShift class to help manage it, it has some methods:  
  - enableChunkingData: to help reduce pressure on system resources I added chunking ability. for example, you cannot store 1M shifts in one iteration no matter how beefy your server is! [probably you can but it will choke the server], with enabling chunks, you can save 1M shifts with a chunk size of for example 1000 to help reduce pressure on Ram, in cost of using more queries.  
  - defineChunkSize  
  - enableDebug: if you enable debug, after saving shift it will show you some debug data like time consumed, memory used, query count, etc.  
  
> If I want to explain how StoreShift class works as simple as possible,  
> I store all relations of each field in a single query and then attach
> relations to shifts later, Something like that ^_O  
  
### Delete shifts  
- not much to say, it will truncate tables  
  
### Query shifts  
also, not much to say, query it as the required task, you can see the route in Postman as an example.  
  
## Console App (CLI)  
what is done here **was not** in the required task, but I really wanted to test what I did!  
I made a big json file, around 519MB it contains 1.35M shifts  
you can feed it to my code like this :

```php artisan storeShifts filename.json```  

with my tests, I run the app for 8 minutes, it generated 1.35M shifts, with maximum ram usage of 210Mb, and no memory leaks, and no problem at all!  
It saved all the data and required relations. YAY!  
  
## Tests  
- there are some tests in test folder  
- you can test code with this command:  
- php artisan test  
- or phpunit  
- they are the same actually :) [php artisan test just add some fancy styling to it]  
  
## Postman  
  
- I added some big Jsons to postman routes for testing, so uploading it  
raw will be time-consuming, I attached it as a compressed file named  
```Parim.postman_collection.7z```  
- the file is Postman Collection version 2.1 (the recommended version by postman)  
  
## Facts:  
1. for the user model, each user has a unique email address and an extra user_name field.  
2. I added a lot of complexity to get better performance, doing this task with Laravel Eloquent Relation helpers is like cutting butter with a knife, really really easy **but** not performant, it makes a lot of queries [I mean A LOT!].  
3. my laptop is a Macbook air 2017, 8GB ram [this is the system I did all these tests].  
4. in the task you were required to also save area relation, but in the whole example, I didn't found a single entry of area with example data so I skipped it but handling it wouldn't be much of a problem.  
5. my solutions state for saving nearly 1,350,000 shifts (1.35M) is:  
- chunks enabled: true  
- chunks size: 6000  
- total Shifts: 1.35M  
- total Queries: 40828  
- memory Usage: 172.93 MB  
- Peak memory usage: 210.45 MB  
- Time usage as Seconds: 501