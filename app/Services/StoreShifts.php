<?php namespace App\Services;

use App\Contracts\ShiftBuildersContract;
use App\Exceptions\ShiftHandlerNotFoundException;
use App\Models\Shift;
use App\Services\Handlers\ShiftHandler;
use Carbon\Carbon;
use Event;
use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Http\JsonResponse;

class StoreShifts implements ShiftBuildersContract
{
    protected bool $chunkEnabled = true;
    protected bool $debug = false;
    protected int $chunkSize = 200;
    protected int $totalQueries = 0;
    protected int $totalShifts = 0;
    protected float $timer = 0;
    protected Helper $helper;
    
    public function __construct()
    {
        $this->helper = new Helper();
        $this->timer  = microtime(true);
    }
    
    /**
     * @throws ShiftHandlerNotFoundException
     */
    public function handler(array $shifts): JsonResponse
    {
        if ($this->debug) {
            // debug measurements
            
            Event::listen(QueryExecuted::class, function () {
                $this->totalQueries++;
            });
            
            $this->totalShifts += count($shifts);
        }
        
        if ($this->chunkEnabled) {
            $shiftChunks = array_chunk($shifts, $this->chunkSize);
            foreach ($shiftChunks as $shiftChunk) {
                $this->store($shiftChunk);
            }
        } else {
            $this->store($shifts);
        }
        
        $output = ['status' => 'done'];
        
        if ($this->debug) {
            // debug measurements
            $output['chunks enabled'] = $this->chunkEnabled;
            
            if ($this->chunkEnabled) {
                $output['chunks size'] = $this->chunkSize;
            }
            
            $output['total Shifts']          = number_format($this->totalShifts);
            $output['total Queries']         = number_format($this->totalQueries);
            $output['memory Usage']          = $this->helper->convertSize(memory_get_usage());
            $output['Peak memory Usage']     = $this->helper->convertSize(memory_get_peak_usage());
            $output['Time usage as Seconds'] = Carbon::parse($this->timer)->diffForHumans();
        }
        
        return response()->json($output);
    }
    
    /**
     * @throws ShiftHandlerNotFoundException
     */
    private function store(array $shifts): void
    {
        $relations    = $this->handleRelations();
        $shiftsToSave = [];
        
        foreach ($shifts as $key => $shift) {
            $shiftsToSave[$key]['type']        = $shift['type'];
            $shiftsToSave[$key]['start']       = $shift['start'];
            $shiftsToSave[$key]['end']         = $shift['end'];
            $shiftsToSave[$key]['rate']        = $shift['rate'];
            $shiftsToSave[$key]['charge']      = $shift['charge'];
            $shiftsToSave[$key]['area']        = $shift['area'];
            $shiftsToSave[$key]['event_id']    = null;
            $shiftsToSave[$key]['location_id'] = null;
            $shiftsToSave[$key]['user_id']     = null;
            $shiftsToSave[$key]['temp_key']    = $key;
            
            $relations['event']->feed($key, $shift['event']);
            $relations['department']->feed($key, $shift['departments']);
            $relations['location']->feed($key, $shift['location']);
            
            $userData = [
                'user_name'  => $shift['user_name'],
                'user_email' => $shift['user_email'],
            ];
            $relations['user']->feed($key, $userData);
        }
        
        // handle event
        $relations['event']->save();
        $shiftsToSave = $relations['event']->handleRelations($shiftsToSave);
        
        $relations['location']->save();
        $shiftsToSave = $relations['location']->handleRelations($shiftsToSave);
        
        $relations['user']->save();
        $shiftsToSave = $relations['user']->handleRelations($shiftsToSave);
        
        // save shifts
        Shift::insert($shiftsToSave);
        
        // handle departments
        $relations['department']->save();
        $relations['department']->handleRelations($shiftsToSave);
        
        // remove shift temp keys
        $this->removeShiftsTempKey();
    }
    
    /**
     * @throws ShiftHandlerNotFoundException
     */
    private function handleRelations(): array
    {
        $output = [];
        
        $output['event']      = ShiftHandler::handle('event');
        $output['department'] = ShiftHandler::handle('department');
        $output['location']   = ShiftHandler::handle('location');
        $output['user']       = ShiftHandler::handle('user');
        
        return $output;
    }
    
    /**
     * remove shift temp keys
     * we use these keys to handle relations
     */
    private function removeShiftsTempKey(): void
    {
        Shift::whereNotNull('temp_key')->update(['temp_key' => null]);
    }
    
    /**
     * decide chunking status builder method
     *
     * @param  bool  $status
     *
     * @return $this
     */
    public function enableChunkingData(bool $status): StoreShifts
    {
        $this->chunkEnabled = $status;
        
        return $this;
    }
    
    /**
     * add chunk size builder method
     *
     * @param  int  $chunkSize
     *
     * @return $this
     */
    public function chunkSize(int $chunkSize): StoreShifts
    {
        $this->chunkSize = $chunkSize;
        
        return $this;
    }
    
    /**
     * add debug status builder method
     *
     * @param  bool  $debugStatus
     *
     * @return $this
     */
    public function enableDebug(bool $debugStatus): StoreShifts
    {
        $this->debug = $debugStatus;
        
        return $this;
    }
}