<?php namespace App\Services\Handlers;

use App\Contracts\ShiftHandlersContract;
use App\Models\Event;
use App\Services\Helper;

class ShiftEventHandler implements ShiftHandlersContract
{
    protected array $data;
    protected array $generatedData;
    protected Helper $helper;
    
    public function __construct()
    {
        $this->helper = new Helper();
        $this->data   = [];
    }
    
    public function feed(int $shiftTempKey, $data = null): void
    {
        if ($data === null) {
            return;
        }
        $eventData = [];
        
        $eventData['shiftTempKey'] = $shiftTempKey;
        $eventData['name']         = $data['name'];
        $eventData['start']        = $this->helper->convertIso8601ToDateTime($data['start']);
        $eventData['end']          = $this->helper->convertIso8601ToDateTime($data['end']);
        
        $this->data[] = $eventData;
    }
    
    public function save(): void
    {
        $eventToSave = $this->helper->uniqueArray($this->generateEventListForSaving());
        
        $savedEvents = Event::query();
        
        foreach ($eventToSave as $event) {
            $savedEvents->orWhere(function ($query) use ($event) {
                $query->whereName($event['name'])
                      ->whereStart($event['start'])
                      ->whereEnd($event['end']);
            });
        }
        
        $savedEvents = $savedEvents->get()->toArray();
        
        foreach ($eventToSave as $key => $event) {
            foreach ($savedEvents as $saved_event) {
                if ($event['name'] === $saved_event['name'] &&
                    $event['start'] === $saved_event['start'] &&
                    $event['end'] === $saved_event['end']
                ) {
                    unset($eventToSave[$key]);
                }
            }
        }
        
        Event::insert($eventToSave);
        
        $savedEvents = Event::query();
        
        foreach ($this->data as $event) {
            $savedEvents->orWhere(function ($query) use ($event) {
                $query->whereName($event['name'])
                      ->whereStart($event['start'])
                      ->whereEnd($event['end']);
            });
        }
        
        $this->generatedData = $savedEvents->get(['id', 'name', 'start', 'end'])->toArray();
    }
    
    public function handleRelations(array $shifts): array
    {
        foreach ($this->data as $datum) {
            foreach ($this->generatedData as $generated_datum) {
                if ($datum['name'] === $generated_datum['name'] &&
                    $datum['start'] === $generated_datum['start'] &&
                    $datum['end'] === $generated_datum['end']
                ) {
                    $shifts[$datum['shiftTempKey']]['event_id'] = $generated_datum['id'];
                    break;
                }
                
            }
        }
        
        return $shifts;
    }
    
    private function generateEventListForSaving(): array
    {
        $output = [];
        
        foreach ($this->data as $key => $event) {
            $output[$key]['name']  = $event['name'];
            $output[$key]['start'] = $event['start'];
            $output[$key]['end']   = $event['end'];
        }
        
        return $output;
    }
}