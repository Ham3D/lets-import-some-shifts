<?php namespace App\Services\Handlers;

use App\Contracts\ShiftHandlersContract;
use App\Models\Location;

class ShiftLocationHandler implements ShiftHandlersContract
{
    
    protected array $data;
    protected array $generatedData;
    
    
    public function __construct()
    {
        $this->data = [];
    }
    
    
    public function feed(int $shiftTempKey, $data): void
    {
        if ($data === null) {
            return;
        }
        
        $locationData = [];
        
        $locationData['temp_key'] = $shiftTempKey;
        $locationData['data']     = $data;
        
        $this->data[] = $locationData;
        
    }
    
    public function save(): void
    {
        $locations = [];
        
        foreach ($this->data as $datum) {
            $locations[] = $datum['data'];
        }
        
        $locations = array_unique($locations);
        if (count($locations) === 0) {
            return;
        }
        
        // check db for existing locations
        $locationsExistInDb = Location::whereIn('title', $locations)
                                      ->get(['title'])->pluck('title')->toArray();
        $locationsToSave    = array_diff($locations, $locationsExistInDb);
        
        // save locations
        $this->saveNewLocationsInDb($locationsToSave);
        
        // get departments from db
        $this->generatedData = Location::whereIn('title', $locations)
                                       ->get()->pluck('id', 'title')
                                       ->toArray();
    }
    
    public function handleRelations(array $shifts): array
    {
        foreach ($this->data as $datum) {
            $shifts[$datum['temp_key']]['location_id'] = $this->getLocationId($datum['data']);
        }
        
        return $shifts;
    }
    
    private function saveNewLocationsInDb(array $locations): void
    {
        $formattedLocationsToSave = [];
        
        foreach ($locations as $location) {
            $formattedLocationsToSave[] = ['title' => $location];
        }
        
        Location::insert($formattedLocationsToSave);
    }
    
    private function getLocationId(string $location): int
    {
        return $this->generatedData[$location];
    }
    
}
