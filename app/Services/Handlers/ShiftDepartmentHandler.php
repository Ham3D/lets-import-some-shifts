<?php namespace App\Services\Handlers;

use App\Contracts\ShiftHandlersContract;
use App\Models\Department;
use App\Models\Shift;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ShiftDepartmentHandler implements ShiftHandlersContract
{
    protected array $data;
    protected array $generatedData;
    
    public function feed(int $shiftTempKey = null, $data = null): void
    {
        if ($data === null) {
            return;
        }
        
        $departmentData = [];
        
        $departmentData['temp_key'] = $shiftTempKey;
        
        foreach ($data as $datum) {
            $departmentData['departments'][] = $datum;
        }
        
        $this->data[] = $departmentData;
    }
    
    public function save(): void
    {
        $departments = [];
        foreach ($this->data as $datum) {
            $departments[] = $datum['departments'];
        }
        
        $departments = array_unique(Arr::flatten($departments));
        
        if (count($departments) === 0) {
            return;
        }
        
        // check db for existing departments
        $departmentsExistInDb = Department::whereIn('title', $departments)
                                          ->get(['title'])->pluck('title')->toArray();
        
        $departmentsToSave = array_diff($departments, $departmentsExistInDb);
        
        // save departments
        $this->saveNewDepartmentsInDB($departmentsToSave);
        
        // get departments from db
        $this->generatedData = Department::whereIn('title', $departments)
                                         ->get()->pluck('id', 'title')
                                         ->toArray();
    }
    
    public function handleRelations(array $shifts): void
    {
        $departmentRelation = [];
        $shiftTempKeys      = [];
        foreach ($this->data as $datum) {
            $shiftTempKeys[] = $datum['temp_key'];
        }
        
        $shiftFromDb = Shift::whereIn('temp_key', $shiftTempKeys)
                            ->get(['id', 'temp_key'])
                            ->pluck(['id'], 'temp_key')
                            ->toArray();
        
        
        $now = Carbon::now();
        
        foreach ($this->data as $item) {
            foreach ($item['departments'] as $department) {
                $row                  = [];
                $row['shift_id']      = $shiftFromDb[$item['temp_key']];
                $row['department_id'] = $this->getDepartmentId($department);
                $row['created_at']    = $now;
                $row['updated_at']    = $now;
                
                $departmentRelation[] = $row;
            }
        }
        
        DB::table('department_shift')->insert($departmentRelation);
    }
    
    /**
     * save departments in db
     *
     * @param  array  $departments
     */
    private function saveNewDepartmentsInDB(array $departments): void
    {
        $formattedDepartmentsToSave = [];
        
        foreach ($departments as $item) {
            $formattedDepartmentsToSave[] = ['title' => $item];
        }
        
        Department::insert($formattedDepartmentsToSave);
    }
    
    private function getDepartmentId(string $departmentTitle): int
    {
        return $this->generatedData[$departmentTitle];
    }
    
}