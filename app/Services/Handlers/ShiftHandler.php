<?php namespace App\Services\Handlers;

use App\Contracts\ShiftHandlersContract;
use App\Exceptions\ShiftHandlerNotFoundException;

class ShiftHandler
{
    /**
     * @throws ShiftHandlerNotFoundException
     */
    public static function handle(string $handlerName): ShiftHandlersContract
    {
        switch ($handlerName) {
            case 'event':
                return new ShiftEventHandler();
            case 'department':
                return new ShiftDepartmentHandler();
            case 'location':
                return new ShiftLocationHandler();
            case 'user':
                return new ShiftUserHandler();
            
            default:
                throw new ShiftHandlerNotFoundException();
        }
    }
}