<?php namespace App\Services\Handlers;

use App\Contracts\ShiftHandlersContract;
use App\Models\User;
use App\Services\Helper;
use Carbon\Carbon;

class ShiftUserHandler implements ShiftHandlersContract
{
    protected Helper $helper;
    protected array $data;
    protected array $generatedData;
    
    public function __construct()
    {
        $this->data   = [];
        $this->helper = new Helper();
    }
    
    public function feed(int $shiftTempKey, $data): void
    {
        if ($data === null || ($data['user_name'] === null && $data['user_email'] === null)) {
            return;
        }
        
        $userData = [];
        
        $userData['temp_key']   = $shiftTempKey;
        $userData['user_name']  = $data['user_name'];
        $userData['user_email'] = $data['user_email'];
        
        $this->data[] = $userData;
    }
    
    public function save(): void
    {
        $users = [];
        foreach ($this->data as $datum) {
            $users[] = [
                'user_name' => $datum['user_name'],
                'email'     => $datum['user_email'],
            ];
        }
        
        $users = $this->helper->uniqueArray($users);
        
        if (count($users) === 0) {
            return;
        }
        
        $userEmails  = $this->getEmails($users);
        $usersToSave = $this->usersNotExistInDb($users, $userEmails);
        
        // save users
        $this->saveNewUsersInDb($usersToSave);
        
        // get users from db
        $this->generatedData = User::whereIn('email', $userEmails)
                                   ->get()->pluck('id', 'email')
                                   ->toArray();
        
    }
    
    public function handleRelations(array $shifts): array
    {
        foreach ($this->data as $datum) {
            $shifts[$datum['temp_key']]['user_id'] = $this->getUserId($datum['user_email']);
        }
        
        return $shifts;
    }
    
    private function getEmails(array $users): array
    {
        $emails = [];
        foreach ($users as $user) {
            if (isset($user['email'])) {
                $emails[] = $user['email'];
            }
        }
        
        return $emails;
    }
    
    private function saveNewUsersInDb(array $users): void
    {
        $now                  = Carbon::now();
        $formattedUsersToSave = [];
        
        foreach ($users as $user) {
            $row               = [];
            $row['user_name']  = $user['user_name'];
            $row['email']      = $user['email'];
            $row['created_at'] = $now;
            $row['updated_at'] = $now;
            
            $formattedUsersToSave[] = $row;
        }
        User::insert($formattedUsersToSave);
    }
    
    private function usersNotExistInDb(array $users, $userEmails): array
    {
        $usersNotExistInDb = [];
        
        $usersExistInDb = User::whereIn('email', $userEmails)
                              ->get(['id', 'email'])->pluck('email')->toArray();
        
        foreach ($users as $user) {
            if ( ! in_array($user['email'], $usersExistInDb, true)) {
                $usersNotExistInDb [] = $user;
            }
        }
        
        return $usersNotExistInDb;
    }
    
    private function getUserId(string $userEmail)
    {
        return $this->generatedData[$userEmail];
    }
    
}