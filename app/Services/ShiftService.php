<?php namespace App\Services;

use App\Http\Resources\ShiftCollection;
use App\Models\Department;
use App\Models\Event;
use App\Models\Location;
use App\Models\Shift;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShiftService
{
    public function getShifts(Request $request): ShiftCollection
    {
        $location = $request->get('location');
        $from     = Carbon::parse($request->get('from'));
        $to       = Carbon::parse($request->get('to'));
        
        $shifts = Shift::with(['departments', 'event', 'user', 'location'])
                       ->whereHas('location', static function ($query) use ($location) {
                           $query->where('title', $location);
                       })
                       ->whereBetween('start', [$from, $to])
                       ->get();
        
        return ShiftCollection::make($shifts);
    }
    
    public function destroyData(): JsonResponse
    {
        Department::truncate();
        DB::table('department_shift')->truncate();
        Event::truncate();
        Location::truncate();
        User::truncate();
        Shift::truncate();
        
        return response()->json(['status' => 'done']);
    }
    
}