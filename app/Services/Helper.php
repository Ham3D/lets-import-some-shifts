<?php namespace App\Services;

use Carbon\Carbon;

class Helper
{
    /**
     * unique multidimensional array using array_intersect_key
     * I actually used unserialize before, but its slower and might cause security risks!
     *
     * @param $array
     *
     * @return array
     */
    public function uniqueArray($array): array
    {
        $result = array_intersect_key($array, array_unique(array_map('serialize', $array)));
        
        foreach ($result as $key => $value) {
            if (is_array($value)) {
                $result[$key] = $this->uniqueArray($value);
            }
        }
        
        return $result;
    }
    
    public function convertIso8601ToDateTime($dateTime): string
    {
        return Carbon::parse($dateTime)->toDateTimeString();
    }
    
    public function convertSize($size): string
    {
        $unit = ['b', 'kb', 'mb', 'gb', 'tb', 'pb'];
        
        return @round($size / (1024 ** ($i = floor(log($size, 1024)))), 2).' '.$unit[$i];
    }
}