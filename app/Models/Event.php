<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin IdeHelperEvent
 */
class Event extends Model
{
    
    public $timestamps = false;
    
    public function shits(): HasMany
    {
        return $this->hasMany(Shift::class);
    }
    
}
