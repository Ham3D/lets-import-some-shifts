<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @mixin IdeHelperShift
 */
class Shift extends Model
{
    public $timestamps = false;
    
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
    
    public function location(): BelongsTo
    {
        return $this->belongsTo(Location::class);
    }
    
    public function event(): BelongsTo
    {
        return $this->belongsTo(Event::class);
    }
    
    public function departments(): BelongsToMany
    {
        return $this->belongsToMany(Department::class);
    }
}
