<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin IdeHelperUser
 */
class User extends Model
{
    
    public function shifts(): HasMany
    {
        return $this->hasMany(Shift::class);
    }
}
