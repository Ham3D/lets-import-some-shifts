<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @mixin IdeHelperLocation
 */
class Location extends Model
{
    
    public $timestamps = false;
    
    public function shifts(): BelongsToMany
    {
        return $this->belongsToMany(Shift::class);
    }
}
