<?php namespace App\Contracts;

interface ShiftBuildersContract
{
    public function enableChunkingData(bool $status);
    
    public function chunkSize(int $chunkSize);
    
    public function enableDebug(bool $debugStatus);
}