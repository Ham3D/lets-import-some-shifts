<?php namespace App\Contracts;

interface ShiftHandlersContract
{
    public function feed(int $shiftTempKey, $data): void;
    
    public function save(): void;
    
    public function handleRelations(array $shifts);
}