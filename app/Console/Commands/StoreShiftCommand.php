<?php

namespace App\Console\Commands;

use App\Exceptions\ShiftHandlerNotFoundException;
use App\Services\StoreShifts;
use Illuminate\Console\Command;
use pcrov\JsonReader\Exception;
use pcrov\JsonReader\InputStream\IOException;
use pcrov\JsonReader\InvalidArgumentException;
use pcrov\JsonReader\JsonReader;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableCellStyle;

class StoreShiftCommand extends Command
{
    protected ProgressBar $progressBar;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storeShifts {file}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'store shifts';
    
    /**
     * Execute the console command.
     *
     * @return int
     * @throws Exception
     * @throws IOException
     * @throws InvalidArgumentException
     * @throws ShiftHandlerNotFoundException
     */
    public function handle(): int
    {
        $this->progressBar = new ProgressBar($this->output);
        $this->progressBar->setBarWidth(50);
        
        $file = $this->argument('file');
        $this->storeShifts($file);
        
        return 0;
    }
    
    /**
     * @throws IOException
     * @throws Exception
     * @throws InvalidArgumentException
     * @throws ShiftHandlerNotFoundException
     */
    private function storeShifts($file): void
    {
        $jsonChunkSize = 10000;
        $index         = 0;
        $shiftsData    = [];
        
        // clear all data
        //$shiftController = new ShiftsController();
        //$shiftController->destroy();
        
        $reader = new JsonReader();
        $reader->open($file);
        
        $reader->read("shifts");
        $depth = $reader->depth();
        
        // init the storeShift class
        $storeShifts = new StoreShifts();
        $storeShifts->enableDebug(true);
        $storeShifts->enableChunkingData(true);
        $storeShifts->chunkSize(200);
        
        // start reading json
        $reader->read();
        do {
            if ($index >= $jsonChunkSize) {
                $this->outputResult($storeShifts->handler($shiftsData)->original);
                
                $index      = 0;
                $shiftsData = [];
            }
            $shiftsData[] = $reader->value();
            $index++;
        }while ($reader->next() && $reader->depth() > $depth);
        
        // handle to remain data in $shiftsData
        if (count($shiftsData) > 0) {
            $this->outputResult($storeShifts->handler($shiftsData)->original);
        }
        
        $reader->close();
        $this->newLine();
        $this->info('done.');
    }
    
    /**
     * sorry for the mess down here
     * some gui work for console x_O
     *
     * @param $data
     */
    private function outputResult($data): void
    {
        $this->newLine();
        system('clear');
        $table = new Table($this->output);
        $table->setHeaderTitle('Import Shifts');
        $table->setHeaders(['Saved Shifts', 'Total Queries', 'Memory Usage', 'Peak memory Usage', 'Time'])
              ->setRows(
                  [
                      [
                          new TableCell(
                              $data['total Shifts'],
                              ['style' => new TableCellStyle(['align' => 'center'])]),
                          new TableCell($data['total Queries'],
                              ['style' => new TableCellStyle(['align' => 'center'])]),
                          new TableCell(
                              $data['memory Usage'],
                              ['style' => new TableCellStyle(['align' => 'center'])]
                          ),
                          new TableCell(
                              $data['Peak memory Usage'],
                              ['style' => new TableCellStyle(['align' => 'center'])]
                          ),
                          new TableCell(
                              $data['Time usage as Seconds'],
                              ['style' => new TableCellStyle(['align' => 'center']),]
                          ),
                      ],
                  ]
              )->render();
        $this->newLine();
        $this->progressBar->advance();
    }
    
}
