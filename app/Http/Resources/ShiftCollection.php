<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShiftCollection extends JsonResource
{
    public static $wrap = 'shifts';
    
    public function toArray($request)
    {
        return ShiftsResource::collection($this);
    }
}
