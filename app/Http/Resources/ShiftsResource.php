<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ShiftsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            "type"        => $this->type,
            "start"       => $this->start,
            "end"         => $this->end,
            "user_name"   => $this->user->user_name,
            'user_email'  => $this->user->email,
            "location"    => $this->location['title'],
            "event"       => EventResource::make($this->event),
            "rate"        => $this->rate,
            "charge"      => $this->charge,
            "area"        => $this->area,
            "departments" => $this->departments->implode('title', ', '),
        ];
    }
}
