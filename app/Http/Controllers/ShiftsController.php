<?php

namespace App\Http\Controllers;

use App\Exceptions\ShiftHandlerNotFoundException;
use App\Http\Requests\ShiftsFilterRequest;
use App\Http\Resources\ShiftCollection;
use App\Services\ShiftService;
use App\Services\StoreShifts;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ShiftsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function search(ShiftsFilterRequest $request): ShiftCollection
    {
        return (new ShiftService())->getShifts($request);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     *
     * @return JsonResponse
     * @throws ShiftHandlerNotFoundException
     */
    public function store(Request $request): JsonResponse
    {
        // destroy all data before each shift store operation?
        $this->destroy();
        
        $shifts = $request->get('shifts');
        
        $storeShifts = new StoreShifts();
        
        // config the Class
        $storeShifts->enableChunkingData(true);
        $storeShifts->chunkSize(200);
        $storeShifts->enableDebug(false);
        
        // handle shifts
        return $storeShifts->handler($shifts);
    }
    
    /**
     * Remove all shifts
     */
    public function destroy(): JsonResponse
    {
        return (new ShiftService())->destroyData();
    }
}
