<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Department
 *
 * @mixin IdeHelperDepartment
 * @property int $id
 * @property string $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Shift[] $shifts
 * @property-read int|null $shifts_count
 * @method static \Database\Factories\DepartmentFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Department whereTitle($value)
 */
	class IdeHelperDepartment extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Event
 *
 * @mixin IdeHelperEvent
 * @property int $id
 * @property string $name
 * @property string $start
 * @property string $end
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Shift[] $shits
 * @property-read int|null $shits_count
 * @method static \Database\Factories\EventFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Event newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Event query()
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Event whereStart($value)
 */
	class IdeHelperEvent extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Location
 *
 * @mixin IdeHelperLocation
 * @property int $id
 * @property string $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Shift[] $shifts
 * @property-read int|null $shifts_count
 * @method static \Database\Factories\LocationFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Location newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Location query()
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Location whereTitle($value)
 */
	class IdeHelperLocation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Shift
 *
 * @mixin IdeHelperShift
 * @property int $id
 * @property string $type
 * @property string $start
 * @property string $end
 * @property int $user_id
 * @property int|null $location_id
 * @property int|null $event_id
 * @property int|null $rate
 * @property int|null $charge
 * @property string|null $area
 * @property int|null $temp_key
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Department[] $departments
 * @property-read int|null $departments_count
 * @property-read \App\Models\Event|null $event
 * @method static \Database\Factories\ShiftFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Shift newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Shift query()
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereEventId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereLocationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereTempKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Shift whereUserId($value)
 */
	class IdeHelperShift extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @mixin IdeHelperUser
 * @property int $id
 * @property string|null $user_name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|User query()
 * @method static \Illuminate\Database\Eloquent\Builder|User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereUserName($value)
 */
	class IdeHelperUser extends \Eloquent {}
}

